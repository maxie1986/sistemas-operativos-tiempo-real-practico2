/* Copyright 2017, XXXX
 *
 *  * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal example source file
 **
 ** This is a mini example of the CIAA Firmware.
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/
#include "eventos.h"       /* <= own header */
#include "task.h"
#include "event_groups.h"

/*==================[macros and definitions]=================================*/
#define EVENTO_TECLA 0x01

typedef struct {
      uint8_t tecla1;
      uint8_t tecla2;
      uint8_t tecla3;
} clave;

typedef struct {
   uint8_t led;
   uint16_t delay;
   uint8_t tec4_ingresada;
   uint8_t alarmaOk;
   EventGroupHandle_t evento;
   uint8_t ingresada[3];
} blinking_t;

static clave pass = {.tecla1 = TEC1, .tecla2 = TEC2, .tecla3 = TEC3};


/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
/** \brief Main function
 *
 * This is the main entry point of the software.
 *
 * \returns 0
 *
 * \remarks This function never returns. Return value is only to avoid compiler
 *          warnings or errors.
 */
void Blinking(void * parametros) {
   blinking_t * valores = parametros;
   while(1) {
      while(xEventGroupWaitBits(valores->evento, EVENTO_TECLA, TRUE, FALSE, valores->delay) == 0);
      Led_On(valores->led);
      xEventGroupWaitBits(valores->evento, EVENTO_TECLA, TRUE, FALSE, valores->delay);
      Led_Off(valores->led);

      if (!valores->alarmaOk) {
           Led_On(RED_LED);  
      }
      valores->tec4_ingresada = 0;
   }
}

void Teclado(void * parametros) {
   blinking_t * valores = parametros;
   uint8_t tecla;
   uint8_t anterior = 0;
   uint8_t intentos_fallidos = 0;
   uint8_t contador_teclas = 0;
   EventGroupHandle_t evento = valores->evento;

   while(1) {
      tecla = Read_Switches();
      if (tecla != anterior) {
         switch(tecla) {
            case TEC1:
                  if (valores->tec4_ingresada == 1) {
                        valores->ingresada[contador_teclas] = TEC1;
                        contador_teclas++;
                        Led_On(YELLOW_LED);
                        vTaskDelay(100);
                        Led_Off(YELLOW_LED);
                  }
                  break;
            case TEC2:
                  if (valores->tec4_ingresada == 1) {
                        valores->ingresada[contador_teclas] = TEC2;
                        contador_teclas++;
                        Led_On(YELLOW_LED);
                        vTaskDelay(100);
                        Led_Off(YELLOW_LED);
                  }
               break;
            case TEC3:
                  if (valores->tec4_ingresada == 1) {
                        valores->ingresada[contador_teclas] = TEC3;
                        contador_teclas++;
                        Led_On(YELLOW_LED);
                        vTaskDelay(100);
                        Led_Off(YELLOW_LED);
                  }
               break;
            case TEC4:
                  xEventGroupSetBits(evento, EVENTO_TECLA);
                  valores->tec4_ingresada = 1;
               break;
         }

            //si se ingresaron 3 teclas recien hacemos la comparacion
            if (contador_teclas == 3) {
                  contador_teclas = 0;
                  if (pass.tecla1 == valores->ingresada[0] && 
                        pass.tecla2 == valores->ingresada[1] && 
                        pass.tecla3 == valores->ingresada[2]) {
                              //prendemos el led y desbloqueamos en la tarea Blinking para no tener que esperar que pasen los 30 seg
                        Led_On(GREEN_LED);
                        Led_Off(valores->led);
                        intentos_fallidos = 0;  
                        valores->alarmaOk = 1;
                        xEventGroupSetBits(evento, EVENTO_TECLA);
                  } else {
                        intentos_fallidos++;
                        if (intentos_fallidos == 3) {
                              Led_On(RED_LED); 
                              Led_Off(RGB_B_LED);
                        } else {
                              Led_Off(RGB_B_LED);
                              Led_On(RGB_R_LED);
                              vTaskDelay(1000);
                              Led_Off(RGB_R_LED);
                              Led_On(RGB_B_LED);
                        }
                  }
            }

         anterior = tecla;
      }
      vTaskDelay(250);
   }
}

int main(void)
{
   EventGroupHandle_t evento;
   static uint8_t ingresada[3] = {0, 0, 0};
   static blinking_t valores = { .led = RGB_B_LED, .delay = 30000, .tec4_ingresada = 0, .alarmaOk = 0, .evento = NULL, .ingresada = NULL };


   /* inicializaciones */
	Init_Leds();
   Init_Switches();
   evento = xEventGroupCreate();
   if (evento != NULL) {
      /*acá va mi programa principal */
      valores.evento = evento;
      valores.ingresada[0] = ingresada[0];
      valores.ingresada[1] = ingresada[1];
      valores.ingresada[2] = ingresada[2];
      xTaskCreate(Teclado, "Teclado", configMINIMAL_STACK_SIZE, (void *) &valores, 1, NULL);
      xTaskCreate(Blinking, "Azul", configMINIMAL_STACK_SIZE, (void *) &valores, 2, NULL);
      vTaskStartScheduler();
   }
   
   for(;;);

	return 0;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

